package com.merapar.HarryPotterDojo;

import com.merapar.HarryPotterDojo.model.Book;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class BookStorage {

    private final List<Book> bookList;

    public BookStorage() {
        bookList = List.of(
                new Book(UUID.randomUUID(), "Philosophers Stone", "J.K. Rowling"),
                new Book(UUID.randomUUID(), "Chamber of secrets", "J.K. Rowling"),
                new Book(UUID.randomUUID(), "Azkaban's prisoner", "J.K. Rowling"),
                new Book(UUID.randomUUID(), "Goblet of fire", "J.K. Rowling"),
                new Book(UUID.randomUUID(), "Order of the phoenix", "J.K. Rowling")
        );
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public List<Book> getBookList(List<UUID> ids) {
        return bookList.stream().filter(b -> ids.contains(b.getId())).collect(Collectors.toList());
    }

    public Optional<Book> getBook(UUID id) {
        return bookList.stream().filter(b -> b.getId().equals(id)).findAny();
    }
}
