package com.merapar.HarryPotterDojo;

import com.merapar.HarryPotterDojo.model.Book;
import com.merapar.HarryPotterDojo.model.BookRequest;
import com.merapar.HarryPotterDojo.model.CartElement;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class BookRequestMapper {
    public List<CartElement> map(List<BookRequest> bookRequests, Function<UUID, Book> bookRetriever){
        return bookRequests.stream().map(b -> map(b, bookRetriever)).collect(Collectors.toList());
    }

    CartElement map(BookRequest bookRequest, Function<UUID, Book> bookRetriever){
        var book = bookRetriever.apply(bookRequest.getId());
        return new CartElement(book, bookRequest.getCount());
    }
}
