package com.merapar.HarryPotterDojo;

import com.merapar.HarryPotterDojo.model.Book;
import com.merapar.HarryPotterDojo.model.BookRequest;
import com.merapar.HarryPotterDojo.model.Cart;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {

    private final BookStorage bookStorage;
    private final PriceCalculator priceCalculator;
    private final BookRequestMapper bookRequestMapper;

    public BookService(BookStorage bookStorage, PriceCalculator priceCalculator, BookRequestMapper bookRequestMapper) {
        this.bookStorage = bookStorage;
        this.priceCalculator = priceCalculator;
        this.bookRequestMapper = bookRequestMapper;
    }

    public Cart getCart(List<BookRequest> bookRequests) {
        var cartElements = bookRequestMapper.map(bookRequests, b -> bookStorage.getBook(b).orElseThrow());
        var price = priceCalculator.calculatePrice(cartElements);
        return new Cart(cartElements, price);
    }

    public List<Book> getBooks() {
        return bookStorage.getBookList();
    }
}
