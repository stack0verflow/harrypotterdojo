package com.merapar.HarryPotterDojo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HarryPotterDojoApplication {

	public static void main(String[] args) {
		SpringApplication.run(HarryPotterDojoApplication.class, args);
	}

}
