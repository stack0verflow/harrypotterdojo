package com.merapar.HarryPotterDojo.model;

import java.util.Map;
import java.util.UUID;

public class BookRequest {

    private final UUID id;
    private final Integer count;

    public BookRequest(UUID id, Integer count) {
        this.id = id;
        this.count = count;
    }

    public UUID getId() {
        return id;
    }

    public Integer getCount() {
        return count;
    }
}
