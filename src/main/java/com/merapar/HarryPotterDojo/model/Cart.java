package com.merapar.HarryPotterDojo.model;

import java.math.BigDecimal;
import java.util.List;

public class Cart {

    private final List<CartElement> cartElements;
    private final BigDecimal price;

    public Cart(List<CartElement> cartElements, BigDecimal price) {
        this.cartElements = cartElements;
        this.price = price;
    }

    public List<CartElement> getCartElements() {
        return cartElements;
    }

    public BigDecimal getPrice() {
        return price;
    }
}
