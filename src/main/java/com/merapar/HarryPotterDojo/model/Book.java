package com.merapar.HarryPotterDojo.model;

import java.util.UUID;

public class Book {

    private final UUID id;
    private final String title;
    private final String author;

    public Book(UUID id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }
}
