package com.merapar.HarryPotterDojo.model;

public class CartElement {
    private final Book book;
    private final Integer count;

    public CartElement(Book book, Integer count) {
        this.book = book;
        this.count = count;
    }

    public Book getBook() {
        return book;
    }

    public Integer getCount() {
        return count;
    }
}
