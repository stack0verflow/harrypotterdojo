package com.merapar.HarryPotterDojo;

import com.merapar.HarryPotterDojo.model.Book;
import com.merapar.HarryPotterDojo.model.BookRequest;
import com.merapar.HarryPotterDojo.model.Cart;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("book")
public class BookController {

    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("cart")
    public Cart getCart(List<BookRequest> bookRequests) {
        return bookService.getCart(bookRequests);
    }

    @GetMapping
    public List<Book> getAllBooks() {
        return bookService.getBooks();
    }
}
